package ru.olegsvs.typocodetest

import android.app.Application
import androidx.work.Configuration
import androidx.work.WorkManager
import ru.olegsvs.typocodetest.data.db.AppDatabase
import ru.olegsvs.typocodetest.data.network.Api
import ru.olegsvs.typocodetest.data.network.NetworkConnectionInterceptor
import ru.olegsvs.typocodetest.data.preferences.PreferenceProvider
import ru.olegsvs.typocodetest.data.repository.PostRepository
import ru.olegsvs.typocodetest.data.worker.PostWorkerFactory
import ru.olegsvs.typocodetest.ui.create.CreatePostViewModelFactory
import ru.olegsvs.typocodetest.ui.fetch.PostsViewModelFactory
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

class App : Application(), KodeinAware {
    override val kodein: Kodein = Kodein.lazy {

        import(androidXModule(this@App))
        bind() from singleton { NetworkConnectionInterceptor(instance()) }
        bind() from singleton { Api(instance()) }
        bind() from singleton { AppDatabase(instance()) }
        bind() from singleton { PreferenceProvider(instance()) }


        bind() from provider { PostRepository(instance(), instance(), instance()) }
        bind() from provider { PostsViewModelFactory(instance()) }
        bind() from provider { CreatePostViewModelFactory(instance()) }
        bind() from provider { PostWorkerFactory(instance()) }

    }

    private val factory: PostWorkerFactory by instance()

    private fun initWorkManager() {
        WorkManager.initialize(this, Configuration.Builder().run {
            setWorkerFactory(factory)
            build()
        })
    }

    override fun onCreate() {
        super.onCreate()
        initWorkManager()
    }
}