package ru.olegsvs.typocodetest.ui.create

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ru.olegsvs.typocodetest.data.repository.PostRepository

class CreatePostViewModelFactory(
    private val repository: PostRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return CreatePostViewModel(repository) as T
    }
}