package ru.olegsvs.typocodetest.ui.fetch

import ru.olegsvs.typocodetest.R
import ru.olegsvs.typocodetest.data.db.entities.Post
import ru.olegsvs.typocodetest.databinding.ItemPostBinding
import com.xwray.groupie.databinding.BindableItem

class PostItem(
    private val post: Post
) : BindableItem<ItemPostBinding>() {
    override fun getLayout(): Int = R.layout.item_post

    override fun bind(viewBinding: ItemPostBinding, position: Int) {
        viewBinding.post = post
    }
}