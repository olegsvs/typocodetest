package ru.olegsvs.typocodetest.ui.create

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import ru.olegsvs.typocodetest.R
import ru.olegsvs.typocodetest.databinding.CreatePostFragmentBinding
import ru.olegsvs.typocodetest.util.hide
import ru.olegsvs.typocodetest.util.show
import ru.olegsvs.typocodetest.util.toast
import kotlinx.android.synthetic.main.create_post_fragment.*
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

class CreatePostFragment : Fragment(), KodeinAware, CreatePostInterface {

    companion object {
        fun newInstance() = CreatePostFragment()
    }

    override val kodein: Kodein by kodein()

    private val factory: CreatePostViewModelFactory by instance()
    private lateinit var viewModel: CreatePostViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: CreatePostFragmentBinding =
            DataBindingUtil.inflate(inflater, R.layout.create_post_fragment, container, false)
        viewModel = ViewModelProviders.of(this, factory).get(CreatePostViewModel::class.java)
        binding.viewmodel = viewModel
        binding.lifecycleOwner = this
        viewModel.createPostInterface = this
        return binding.root
    }


    override fun onSuccess() {
        progress_bar.hide()
        context?.toast(getString(R.string.post_added_hint))
        findNavController().navigate(R.id.postsFragment)
    }

    override fun onFailed(message: String) {
        progress_bar.hide()
        context?.toast(message)
    }

    override fun onPostPone() {
        progress_bar.hide()
        context?.toast(getString(R.string.post_added_later_hint))
        findNavController().navigate(R.id.postsFragment)
    }

    override fun onRequest() {
        progress_bar.show()
    }
}
