package ru.olegsvs.typocodetest.ui.fetch

import android.view.View
import androidx.lifecycle.ViewModel
import androidx.navigation.findNavController
import ru.olegsvs.typocodetest.R
import ru.olegsvs.typocodetest.data.repository.PostRepository
import ru.olegsvs.typocodetest.util.lazyDeferred

class PostsViewModel(
    private val repository: PostRepository
) : ViewModel() {
    val quotes by lazyDeferred {
        repository.getPosts()
    }

    fun onFabClick(view: View) {
        view.findNavController().navigate(R.id.createPostFragment)
    }
}