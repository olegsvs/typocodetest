package ru.olegsvs.typocodetest.data.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import ru.olegsvs.typocodetest.data.db.AppDatabase
import ru.olegsvs.typocodetest.data.db.entities.Post
import ru.olegsvs.typocodetest.data.network.Api
import ru.olegsvs.typocodetest.data.network.SafeApiRequest
import ru.olegsvs.typocodetest.data.preferences.PreferenceProvider
import ru.olegsvs.typocodetest.util.Coroutines
import ru.olegsvs.typocodetest.util.now
import ru.olegsvs.typocodetest.util.timediff
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext


const val MINIMUM_INTERVAL = 300

class PostRepository(
    private val api: Api,
    private val db: AppDatabase,
    private val prefs: PreferenceProvider
) : SafeApiRequest() {
    private val posts = MutableLiveData<List<Post>>()

    init {
        posts.observeForever {
            savePosts(it)
        }
    }

    suspend fun getPosts(): LiveData<List<Post>> {
        return withContext(Dispatchers.IO) {
            fetchPosts()
            db.getPostDao().getPosts()
        }
    }

    suspend fun addPost(title: String, body: String) {
        return withContext(Dispatchers.IO) {
            val response = apiRequest { api.addPost(Post(null, null, title, body)) }
            db.getPostDao().addPost(response)
        }
    }


    private suspend fun fetchPosts() {
        val lastSavedAt = prefs.getLastSavedAt()
        if (lastSavedAt == null || isFetchNeeded(lastSavedAt)) {
            val response = apiRequest { api.getPosts() }
            posts.postValue(response)
        }
    }

    private fun savePosts(posts: List<Post>) {
        Coroutines.io {
            prefs.saveLastSavedAt(now())
            db.getPostDao().savePosts(posts)
        }
    }

    private fun isFetchNeeded(lastSavedTime: String): Boolean {
        return timediff(lastSavedTime, now()) > MINIMUM_INTERVAL
    }
}