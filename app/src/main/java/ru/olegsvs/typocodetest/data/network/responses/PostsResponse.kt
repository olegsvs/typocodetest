package ru.olegsvs.typocodetest.data.network.responses

import ru.olegsvs.typocodetest.data.db.entities.Post

data class PostsResponse(
    val posts: List<Post>
)