package ru.olegsvs.typocodetest.ui.fetch

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import ru.olegsvs.typocodetest.R
import ru.olegsvs.typocodetest.data.db.entities.Post
import ru.olegsvs.typocodetest.databinding.PostsFragmentBinding
import ru.olegsvs.typocodetest.util.Coroutines
import ru.olegsvs.typocodetest.util.hide
import ru.olegsvs.typocodetest.util.show
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import kotlinx.android.synthetic.main.posts_fragment.*
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

class PostsFragment : Fragment(), KodeinAware {

    companion object {
        fun newInstance() = PostsFragment()
    }

    override val kodein: Kodein by kodein()
    private val factory: PostsViewModelFactory by this.instance()
    private lateinit var viewModel: PostsViewModel


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: PostsFragmentBinding =
            DataBindingUtil.inflate(inflater, R.layout.posts_fragment, container, false)
        viewModel = ViewModelProviders.of(this, factory).get(PostsViewModel::class.java)
        binding.viewmodel = viewModel
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindUI()
    }

    private fun bindUI() = Coroutines.main {
        progress_bar.show()
        viewModel.quotes.await().observe(viewLifecycleOwner, Observer {
            progress_bar.hide()
            initRecyclerView(it.toPostItem())
        })
    }

    private fun initRecyclerView(postItems: List<PostItem>) {
        val mAdapter = GroupAdapter<GroupieViewHolder>().apply {
            addAll(postItems)
        }
        recycler_view.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            setHasFixedSize(true)
            adapter = mAdapter
        }
    }

    private fun List<Post>.toPostItem(): List<PostItem> {
        return this.map {
            PostItem(it)
        }
    }

}
