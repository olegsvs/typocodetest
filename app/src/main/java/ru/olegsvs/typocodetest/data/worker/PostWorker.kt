package ru.olegsvs.typocodetest.data.worker

import android.content.Context
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import ru.olegsvs.typocodetest.data.repository.PostRepository
import ru.olegsvs.typocodetest.util.ApiException

class PostWorker(
    private val context: Context,
    private val params: WorkerParameters,
    private val repository: PostRepository
) :
    CoroutineWorker(context, params) {


    override suspend fun doWork(): Result {
        try {
            val title = inputData.getString("title")
            val body = inputData.getString("body")
            try {
                repository.addPost(title!!, body!!)
            } catch (e: ApiException) {
                return Result.failure()
            }
            return Result.success()
        } catch (error: Throwable) {
            return Result.failure()
        }
    }
}