package ru.olegsvs.typocodetest.ui.fetch

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ru.olegsvs.typocodetest.data.repository.PostRepository

class PostsViewModelFactory(
    private val repository: PostRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return PostsViewModel(repository) as T
    }
}